#!/usr/bin/make -f

PREFIX     = 	/usr
DPATH 	   =	$(DESTDIR)$(PREFIX)/bin
DAPP	   =    $(DESTDIR)$(PREFIX)/share/applications
DEL 	   =  	rm -f
INSTALLD   =    install -Dm755 -t
INSTDSKP   =    install -Dm644 -t
	
install:
	$(INSTALLD) $(DPATH) installapps manjaro-software-install-tool
	$(INSTDSKP) $(DAPP) manjaro-software-install-tool.desktop
	
uninstall:
	$(DEL) $(DPATH)/manjaro-software-install-tool.py
	$(DEL) $(DPATH)/installapps.sh
	$(DEL) $(DAPP)/manjaro-software-install-tool.desktop

PHONY: install uninstall
